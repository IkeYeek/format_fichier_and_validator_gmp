import sys
import re

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Utilisation: ./validator.py mon-fichier.csv")
        exit(64)
    hasError = False
    with open(sys.argv[1]) as csvFile:
        firstLine = csvFile.readline().strip()
        firstLineRegex = re.compile(r"^(\d+((\.)\d+)?|\"\d+((\.|,)\d+)?\") ?, ?(\d+((\.)\d+)?|\"\d+((\.|,)\d+)?\")$")
        lineRegex = re.compile(r"^[a-z_\d ]+( ?, ?(\d+(\.\d+)?|\"\d+((\.|,)\d+)?\")){2} ?, ?(C(ARRE|ERCLE) ?, ?(\d+(\.\d+)?|\"\d+((\.|,)\d+)?\")|(RECTANGLE) ?, ?(\d+(\.\d+)?|\"\d+((\.|,)\d+)?\") ?, ?(\d+(\.\d+)?|\"\d+((\.|,)\d+)?\"))", re.I)
        if firstLineRegex.fullmatch(firstLine) == None:
            hasError = True
            print("La première ligne n'est pas valide (taille de l'entrepot)")
        else:
            lines = list(map(lambda line: line.strip(), csvFile.readlines()))
            for index, line in enumerate(lines):
                if lineRegex.fullmatch(line) == None:
                    hasError = True
                    print(f"Erreur ligne {index}.\n\t{line}")

    if hasError:
        print("Merci de corriger le fichier !")
            
