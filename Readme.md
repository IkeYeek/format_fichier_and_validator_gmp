## Description rapide du format de fichier
L'objectif est de pouvoir représenter facilement une pièce rectangulaire (ici l'atelier GMP) ainsi que les différentes machines qui y sont installées.

Le but ici n'est que de voir comment organiser ces données et de valider ces dernières. 

---

### Format de fichiers
On stock tout dans un fichier CSV. C'est plus simple car directement prit en charge sur excel.

La première ligne représente simplement la taille de l'atelier.

`LARGEUR,LONGUEUR`
 \- Ici, LARGEUR et LONGUEUR sont deux nombres décimaux (avec éventuellement uniquement une partie entière, mais traités comme des floats)

Ensuite, les n lignes suivantes représentent les n machines à venir. L'ordre n'est pas particulièrement important.

`NOM_MACHINE,COORD_X,COORD_Y,FORME,[LARGEUR,LONGEUR|TAILLE/RADIUS]`
 \- Ici, c'est légèrement moins trivial. On commence par le nom de la machine en question, suivi de sa forme. Sa forme représente, de manière explicit, sa forme dans l'espace, à savoir `RECTANGLE, CARRE ou CERCLE`.

 Ensuite, nous avons soit deux valeurs, dans le cas d'un rectangle, qui corresponderont respectivement à sa longueur et sa largeur, soit une seule pour un carré / un cercle (la taille unique de ses 4 côtés ou bien le radius du cercle)

 Nous aurions donc:

"

`LARGEUR_ATELIER,LONGUEUR_ATELIER`

`MACHINE_1,COORD_X_MACHINE_1,COORD_Y_MACHINE_1,CARRE,5.5`

`MACHINE_2,COORD_X_MACHINE_2,COORD_Y_MACHINE_2,CERCLE,3.14159265`

`MACHINE_3,COORD_X_MACHINE_3,COORD_Y_MACHINE_3,RECTANGLE,5.3,4`


"

Et de manière plus réaliste 

"

`13,8.5`

`FRAISEUSE 1,"4,5",5,CARRE,"5,5"`

`DECOUPEUSE_1,8,4.3,CERCLE,"3,14159265"`

`BRAS_ROBOT_1,12,4,RECTANGLE,"0,5",2`

"

Attention, comme les fichiers CSV représentent les séparations entre les cellules d'une même ligne par des virgules, si vous utilisez une virgule comme représentation de la décimale dans excel, il vous convertira 4,3 en "4,3", mais si vous remplissez les fichiers à la main, pensez à ces guillemets (ou mettez directement 4.3). Je gère ça de manière simpliste en changeant les "," en "." avant la conversion.



